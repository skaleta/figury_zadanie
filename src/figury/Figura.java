/**
 * 
 */
package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import javax.swing.JFrame;

//
public abstract class Figura implements Runnable, ActionListener {

	// wspolny bufor
	protected Graphics2D buffer;
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;
	// przesuniecie
	public int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	private int delay;
	private int width;
	private int height;
	private Color clr;
	//public static int dalej;
	//public boolean go=true;
	protected static final Random rand = new Random();

	public Figura(Graphics2D buf, int del, int w, int h) {
		delay = del;
		width = w;
		height = h;
		dx = 1 + rand.nextInt(5);
		dy = 1 + rand.nextInt(5);
		sf = 1 + 0.05 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		// reszta musi być zawarta w realizacji klasy Figure
		// (tworzenie figury i przygotowanie transformacji)

	}

	@Override
	public void run() {
		// przesuniecie na srodek
		aft.translate(100, 100);
		area.transform(aft);
		shape = area;
		while (true) {
			// przygotowanie nastepnego kadru
			if(AnimPanel.timer.isRunning())
			shape = nextFrame();
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
		

	}

	protected Shape nextFrame() {
		// zapamietanie na zmiennej tymczasowej
		// aby nie przeszkadzalo w wykreslaniu
		buffer = AnimPanel.buffer;
		width = AnimPanel.width;
		height = AnimPanel.height;

		area = new Area(area);
		aft = new AffineTransform();

		Rectangle2D bounds = area.getBounds();
		double cx =  bounds.getCenterX() ;
		double cy =  bounds.getCenterY() ;
		
		// odbicie

			if (cx-bounds.getWidth()/2<= 0)
			{
				if(dx<0)
				dx = -dx;
			}	
			if(cx+bounds.getWidth()/2>= width)
			{
				if(dx>0)
				dx = -dx;
			}
			if (cy-bounds.getHeight()/2 <= 0)
			{	
				if(dy<0)
				dy = -dy;
			}
			if( cy+bounds.getHeight()/2 >= height)
			{
				if(dy>0)
				dy = -dy;
			}
			
		// zwiekszenie lub zmniejszenie
		// konstrukcja przeksztalcenia

		aft.translate(dx, dy);
		if (bounds.getHeight() > height / 6 || bounds.getHeight()> width/10 || bounds.getWidth() > height / 6 || bounds.getWidth()> width/10) 
		{
			if(sf>1.0)
			{
								sf = 1 / sf;

			}
		}
		 if( bounds.getHeight() < 10 || bounds.getWidth() < 10  )
		{			if(sf<1.0)
		{
								sf = (1 / sf);	

		}
		}
		aft.translate(cx, cy);
		aft.scale(sf, sf);
		aft.rotate(an);
		aft.translate(-cx, -cy);
		// przeksztalcenie obiektu

		area.transform(aft);
		return area;
	}
	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		buffer.setColor(clr.brighter());
		buffer.fill(shape);
		// wykreslenie ramki
		buffer.setColor(clr.darker());
		buffer.draw(shape);
	}

}
