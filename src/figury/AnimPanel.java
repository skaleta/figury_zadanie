package figury;


import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;


public class AnimPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// bufor
	Image image ;
	// wykreslacz ekranowy
	 Graphics2D device;
	// wykreslacz bufora
	  static Graphics2D buffer;
	private int delay = 70;
	public Figura fig;
	static Timer timer;
public int klik=0;
	//private static int numer = 0;
	//public LinkedList <Figura> lista;
	static int width;
	static int height;
	public AnimPanel() {
		super();
		//lista= new LinkedList<>();
		timer = new Timer(delay, this);
		timer.start();
	}

	public void initialize() {
		 width = getWidth();
		 height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//buffer.setBackground(Color.WHITE);
		//device.setBackground(Color.WHITE);
	}

	void addFig(int indeks) {
		//Figura fig;
		if (indeks == 0)
			 fig = new Kwadrat(buffer, delay, getWidth(), getHeight());
		else if (indeks == 1)
			 fig = new Elipsa(buffer, delay, getWidth(), getHeight());
		else if (indeks == 2)
			 fig = new Trojkat(buffer, delay, getWidth(), getHeight());
		else if (indeks == 3)
			 fig = new Rownoleglobok(buffer, delay, getWidth(), getHeight());
		//lista.add(fig);
		timer.addActionListener(fig);
		new Thread(fig).start();
	}
	void animate() {
		//klik++;
		if (timer.isRunning()) 
		{
			timer.stop();

			
		} 
		else 
		{
			timer.start();

		}

		}
	

	@Override
	public void actionPerformed(ActionEvent e) {	
		device.drawImage(image,0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());

	}
	
}
