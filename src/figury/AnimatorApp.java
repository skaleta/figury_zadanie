package figury;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ActionEvent;

public class AnimatorApp extends JFrame {

	/**
	  	-/**

17 		- * @author tb

18 		- *
	 */
	private static final long serialVersionUID = 1L;
	public static int kil;
	private static JPanel contentPane;
	private  AnimPanel kanwa;
	public static int ww;
	public static int wh;
	private static JComboBox<String> box;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
					} 
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param delay 
	 */
	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		String[] tablica = {"Kwadrat", "Elipsa", "Trojkat","Rownoleglobok"};
		 ww = 450; wh = 300;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		AnimPanel kanwaa = new AnimPanel();
		box= new JComboBox<String>(tablica);
		box.setBounds(100, 239, 93, 23);
		kanwaa.setBounds(10, 11, 422, 219);
		kanwa=kanwaa;
		contentPane.add(kanwa);
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				kanwa.initialize();
			}
		});

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig(box.getSelectedIndex());
			}
		});
		btnAdd.setBounds(10, 239, 80, 23);
		contentPane.add(btnAdd);
		
		JButton btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//kanwa.setBackground(Color.WHITE);
				kanwa.animate();
			}
		});
		btnAnimate.setBounds(200, 239, 100, 23);
		contentPane.add(box);

		contentPane.add(btnAnimate);
		getContentPane().addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e){
		//	Component c  = (Component)e.getSource();
			kil++;
if(kil>1)
{
	kanwa.setBounds(10, 11, contentPane.getWidth()-12, contentPane.getHeight()-42);
	SwingUtilities.invokeLater(new Runnable() {
		
		@Override
		public void run() {
			kanwa.initialize();
		}
	});
	btnAdd.setBounds(10, contentPane.getHeight()-22, 80, 23);	
	btnAnimate.setBounds(200, contentPane.getHeight()-22, 100, 23);
	box.setBounds(100, contentPane.getHeight()-22, 93, 23);
}

			}
		});
		setBackground(Color.WHITE);
		
	}
	
}
